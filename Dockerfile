# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:lts as build-stage
WORKDIR /ci-dir
COPY package*.json /ci-dir/
RUN npm ci
COPY ./ /ci-dir/
RUN npm run build:prod -- --output-path=./dist/app
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:stable
COPY --from=build-stage /ci-dir/dist/app/ /usr/share/nginx/html
# Copy the default nginx.conf
COPY --from=build-stage /ci-dir/nginx.conf /etc/nginx/conf.d/default.conf
