import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { StreamAssetsModule } from '../stream-assets/stream-assets.module';

@NgModule({
  declarations: [],
  imports: [SharedModule, StreamAssetsModule],
})
export class CoreModule {}
