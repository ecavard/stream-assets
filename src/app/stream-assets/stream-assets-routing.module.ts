import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BackgroundComponent } from './background/background.component';
import { BorderBoxComponent } from './border-box/border-box.component';
import { CamBackgroundComponent } from './cam-background/cam-background.component';
import { TitleScreenComponent } from './title-screen/title-screen.component';

const routes: Routes = [
  {
    path: 'background',
    pathMatch: 'full',
    component: BackgroundComponent,
  },
  {
    path: 'border-box',
    pathMatch: 'full',
    component: BorderBoxComponent,
  },
  {
    path: 'cam-background',
    pathMatch: 'full',
    component: CamBackgroundComponent,
  },
  {
    path: 'title-screen',
    pathMatch: 'full',
    component: TitleScreenComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StreamAssetsRoutingModule {}
