import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StreamAssetsRoutingModule } from './stream-assets-routing.module';

import { BackgroundComponent } from './background/background.component';
import { CamBackgroundComponent } from './cam-background/cam-background.component';
import { TitleScreenComponent } from './title-screen/title-screen.component';
import { BorderBoxComponent } from './border-box/border-box.component';

@NgModule({
  declarations: [
    BackgroundComponent,
    CamBackgroundComponent,
    TitleScreenComponent,
    BorderBoxComponent,
  ],
  imports: [CommonModule, StreamAssetsRoutingModule],
})
export class StreamAssetsModule {}
